module.exports = {
   'Login airtasker : Mobile size window' : client => {
    const mobileHomePage = client.page.mobileHomePage();
    const popupPage = client.page.loginPopup();
    const dashboardPage = client.page.dashboardPage();
    
    mobileHomePage.navigate();
    client.resizeWindow(600,800);
    mobileHomePage.waitForElementVisible('@navMenu');
    mobileHomePage.assert.title('Hire skilled people & earn extra money today on Airtasker.com');
    mobileHomePage.assert.visible('@navMenu', 'Expect the nav menu to be visible for mobile');
    mobileHomePage.click('@navMenu');
    mobileHomePage.click('@loginButton');

    popupPage.waitForElementVisible('@email');
    popupPage.setValue('@email', 'brianlo777@gmail.com');
    popupPage.setValue('@password', 'Password1');
    popupPage.click('@login');
    
    dashboardPage.waitForElementVisible('@dashboardTitle', 'We are at the dashboard page');
    dashboardPage.assert.urlEquals("https://www.airtasker.com/account/dashboard/");
    client.end()
   }
 };
 