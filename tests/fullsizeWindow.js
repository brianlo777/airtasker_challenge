module.exports = {
   'Login airtasker : Desktop window' : client => {
    const homePage = client.page.homePage();
    const popupPage = client.page.loginPopup();
    const dashboardPage = client.page.dashboardPage();

    homePage.navigate();
    client.resizeWindow(1000,800)
    homePage.waitForElementVisible('@loginButton');
    homePage.assert.title('Hire skilled people & earn extra money today on Airtasker.com');
    homePage.assert.visible('@loginButton', 'Expect the login button to be visible');
    homePage.click('@loginButton');

    popupPage.waitForElementVisible('@email');
    popupPage.setValue('@email', 'brianlo777@gmail.com');
    popupPage.setValue('@password', 'Password1');
    popupPage.click('@login');
    
    dashboardPage.waitForElementVisible('@dashboardTitle', 'We are at the dashboard page');
    dashboardPage.assert.urlEquals("https://www.airtasker.com/account/dashboard/");
    client.end()
   }
 };
 