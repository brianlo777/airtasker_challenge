# README #
This automation script tests the login feature on the Airtasker homepage using the page object model for reusability.
We spin up a selenium standalone server as part of these tests then run two acceptance tests for the login functionality for desktop and for mobile.

### What is this repository for? ###

* Demonstrating acceptance testing on the Airtasker homepage for desktop and mobile page.

### How do I get set up? ###

* Install Chrome Browser
* Have Node installed. This is tested on Node 8.4.
* clone this repository git clone brianlo777@bitbucket.org:brianlo777/airtasker_challenge.git
* npm install
* npm install -g nightwatch
* nightwatch (We should now see Chrome automating the tests and view Mocha test results, 12 assertions should pass)

Note: These instructions worked on a Mac. Your milage may vary on a Windows box.