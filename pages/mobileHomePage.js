module.exports = {
   elements: {
      loginButton: {
       selector: '#menu-popup > div > div > a.button.login-link.mobile'
     },
     navMenu: {
       selector: 'a[class="sub-menu-tab"]'
     }
   },
   url: 'http://www.airtasker.com'
 };
 