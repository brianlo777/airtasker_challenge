module.exports = {
   elements: {
      email: {
         selector: 'input[type="email"]'
      },
      password: {
         selector: 'input[type="password"]'
     },
     login: {
         selector: 'button[id="login-button"]'
     }
   }
 };